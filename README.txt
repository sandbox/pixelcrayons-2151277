Drupal wp_blog_block module:
------------------------
Maintainers:
  pixelcrayons (https://drupal.org/user/685088)
Requires - Drupal 7
License - GPL (see LICENSE)


Overview:
--------
wordpress blog block is a module which create a block with slider.
This module allows for integration of feed data display in a block with slider configuration into Drupal.


Features:
---------

The wordpress blog block module:

* Excellent integration with wordpress feed
* Different type of setting is available in admin for slider

Installation:
------------
1. Download and unpack the http://bxslider.com/ plugin in "sites/all/libraries".
   Link: http://bxslider.com/lib/jquery.bxslider.zip
2. Download and unpack the wp blog block module directory in your modules folder
   (this will usually be "sites/all/modules/").
3. Go to "Administer" -> "Modules" and enable the module.


Configuration:
-------------
Go to "Structure" -> "Blocks" -> "Wordpress: Blog Block" to find
all the configuration options.

